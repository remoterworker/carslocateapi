using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CarRentalAPI
{
    /// <summary>
    /// Clase principal que contiene el punto de entrada de la aplicación.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Punto de entrada principal de la aplicación.
        /// </summary>
        /// <param name="args">Argumentos de línea de comandos</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Configura el hospedador de la aplicación.
        /// </summary>
        /// <param name="args">Argumentos de línea de comandos</param>
        /// <returns>Un IHostBuilder para configurar la aplicación</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
