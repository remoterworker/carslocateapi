
# Project WEB API Cars-Locate

El proyecto fue eleboarado con tecnologia
.NET 8.0 y lenguaje C#. 

caso:
¿Cuál es el reto?
El Sistema de Búsqueda de Vehículos para Miles Car Rental.
1. Miles Car Rental, una empresa líder en la industria del alquiler de vehículos,
busca implementar un sistema de búsqueda avanzado que permita a sus
clientes encontrar vehículos disponibles de manera eficiente y precisa. Este
sistema se diseñará para cumplir con los criterios específicos de búsqueda que
requiere la empresa, asegurando una experiencia óptima para sus usuarios.
Los vehículos disponibles se deben retornar en base a los siguientes criterios:
a. Localidad de Recogida: Los clientes podrán especificar la localidad
desde donde desean recoger el vehículo. Esta información será
fundamental para determinar la disponibilidad de vehículos en esa
ubicación.
b. Localidad de Devolución: Además de la localidad de recogida, los
usuarios podrán indicar la localidad donde desean devolver el vehículo.
Esto permitirá calcular la disponibilidad y opciones de devolución en
función de la ubicación deseada.
c. Carros Disponibles para este Mercado: El sistema tomará en cuenta
tanto la localidad de recogida como la ubicación del cliente para definir
el mercado correspondiente. En base a este mercado, se mostrarán
únicamente los vehículos disponibles y adecuados para esa área
específica.

# Arquitectura de la API


Estructura de archivos de la Web API y Arquitectura de Capas

-Controllers: Carpeta que contendrá los controladores de la Web API.

-Models: Carpeta que contendrá los modelos de datos.

-Services: Carpeta que contendrá la lógica de negocio.

-Repositories: Carpeta que contendrá la interacción con la base de datos.

- CarRentalAPI
  - Controllers
    - CarController.cs
  - Models
    - CarModel.cs
    - LocalidadModel.cs
  - Services
    - CarService.cs
  - Repositories
    - CarRentalDBContext.cs
    - ICarRepository.cs
    - CarRepository.cs
  - appsettings.json
  - Program.cs
  - Startup.cs

Aquí está una breve explicación de cada carpeta y archivo:

Controllers: Esta carpeta contendrá los controladores de la Web API, que son responsables de recibir las solicitudes HTTP y devolver las respuestas adecuadas.

Models: Aquí se encuentran las clases de modelo que representan los datos de la aplicación. En este caso,  un modelo de carro (CarModel) que representa la estructura de un carro en la base de datos y un modelo de localidad .

Services: En esta carpeta se colocan las clases que contienen la lógica de negocio de la aplicación.

Repositories: Aquí se encuentran las clases que interactúan directamente con la base de datos. El repositorio de carros (CarRepository) implementa la lógica para acceder y manipular los datos de carro en la base de datos. La interfaz ICarRepository define los métodos que deben ser implementados por el repositorio.

appsettings.json: Este archivo contiene la configuración de la aplicación, como la cadena de conexión a la base de datos y demas configuraciones específicas.

Program.cs: Este archivo contiene el punto de entrada de la aplicación. Es responsable de configurar el host de la aplicación y arrancarla.

Startup.cs: Este archivo contiene la configuración inicial de la aplicación, como la configuración del enrutamiento, la inyección de dependencias y la configuración de los servicios middleware.

