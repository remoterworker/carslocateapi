﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarRentalAPI.Models;
using CarRentalAPI.Repositories;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace CarRentalAPI.Services
{
    /// <summary>
    /// Interfaz para definir operaciones relacionadas con Carros.
    /// </summary>
    public interface ICarService
    {
        Task<List<CarModel>> GetAvailableCarsAsync(int pickupLocation, int returnLocation);
        Task<CarModel> GetCarDetailsAsync(int id);
        // Otros métodos relacionados con operaciones de Carros
    }

    /// <summary>
    /// Excepción personalizada para manejar errores en el servicio de Carros.
    /// </summary>
    public class ServiceException : Exception
    {
        public ServiceException(string message) : base(message)
        {
        }

        public ServiceException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    /// <summary>
    /// Servicio para gestionar operaciones relacionadas con vehículos.
    /// </summary>
    public class CarService : ICarService
    {
        private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository ?? throw new ArgumentNullException(nameof(carRepository));
        }

        public async Task<List<CarModel>> GetAvailableCarsAsync(int pickupLocation, int returnLocation)
        {
            try
            {
                return await _carRepository.GetAvailableCarsAsync(pickupLocation, returnLocation);
            }
            catch (Exception ex)
            {
                // Manejo de excepciones.
                throw new ServiceException("Error al obtener Carros disponibles", ex);
            }
        }

        public async Task<CarModel> GetCarDetailsAsync(int id)
        {
            try
            {
                return await _carRepository.GetCarDetailsAsync(id);
            }
            catch (Exception ex)
            {
                // Manejo de excepciones.
                throw new ServiceException($"Error al obtener detalles del vehículo con ID {id}", ex);
            }
        }

   
    }
}
