﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using CarRentalAPI.Repositories;
using CarRentalAPI.Services;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CarRentalAPI
{
    /// <summary>
    /// Clase de inicio de la aplicación.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Constructor de la clase Startup.
        /// </summary>
        /// <param name="configuration">Configuración de la aplicación</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Propiedad de solo lectura para la configuración de la aplicación.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Configuración de servicios de la aplicación.
        /// </summary>
        /// <param name="services">Colección de servicios de la aplicación</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Configuración de la cadena de conexión de la base de datos
            services.AddDbContext<CarRentalDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Registro de servicios
            services.AddScoped<ICarRepository, CarRepository>();
            services.AddScoped<ICarService, CarService>();

            // Configuración de CORS
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                               .AllowAnyMethod()
                               .AllowAnyHeader();
                    });
            });

            services.AddControllers();

            // Configuración de la documentación de Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CarRentalAPI", Version = "v1" });
            });
        }

        /// <summary>
        /// Configuración del pipeline de solicitud HTTP.
        /// </summary>
        /// <param name="app">Constructor de aplicaciones HTTP</param>
        /// <param name="env">Entorno de hospedaje de la aplicación</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CarRentalAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("AllowAll");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
