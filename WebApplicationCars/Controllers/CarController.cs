﻿using Microsoft.AspNetCore.Mvc;
using CarRentalAPI.Models;
using CarRentalAPI.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace CarRentalAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarController : ControllerBase
    {
        private readonly ICarService _carService;

        public CarController(ICarService carService)
        {
            _carService = carService ?? throw new ArgumentNullException(nameof(carService));
        }

        /// <summary>
        /// Endpoint para buscar Carros disponibles según la localidad de recogida y devolución.
        /// </summary>
        /// <param name="pickupLocation">ID de la localidad de recogida</param>
        /// <param name="returnLocation">ID de la localidad de devolución</param>
        /// <returns>Lista de Carros disponibles</returns>
        [HttpGet("{pickupLocation}/{returnLocation}")]
        public async Task<ActionResult<List<CarModel>>> GetAvailableCars(int pickupLocation, int returnLocation)
        {
            try
            {
                var availableCars = await _carService.GetAvailableCarsAsync(pickupLocation, returnLocation);
                return Ok(availableCars);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        /// <summary>
        /// Endpoint para obtener detalles de un carro por su ID.
        /// </summary>
        /// <param name="id">ID del carro</param>
        /// <returns>Detalles del carro</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CarModel>> GetCarDetails(int id)
        {
            try
            {
                var car = await _carService.GetCarDetailsAsync(id);
                if (car == null)
                    return NotFound($"No se encontró ningún vehículo con el ID {id}");

                return Ok(car);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }
}

