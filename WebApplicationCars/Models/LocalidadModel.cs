﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace CarRentalAPI.Models
{
    /// <summary>
    /// Modelo que representa una localidad.
    /// </summary>
    public class LocalidadModel
    {
        /// <summary>
        /// Identificador único de la localidad.
        /// </summary>
        [Key]
        public int LocalidadID { get; set; }

        /// <summary>
        /// Nombre de la localidad.
        /// </summary>
        [Required]
        public string Nombre { get; set; }

        // Propiedad de navegación para representar la relación con los Carros
        public List<CarModel> Cars { get; set; }
    }
}
