﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRentalAPI.Models
{
    /// <summary>
    /// Modelo que representa un carro.
    /// </summary>
    public class CarModel
    {
        /// <summary>
        /// Identificador único del Carro.
        /// </summary>
        [Key]
        public int CarId { get; set; }

        /// <summary>
        /// Marca del Carro.
        /// </summary>
        [Required]
        public string Marca { get; set; }

        /// <summary>
        /// Modelo del Carro.
        /// </summary>
        [Required]
        public string Modelo { get; set; }

        /// <summary>
        /// Año de fabricación del Carro.
        /// </summary>
        [Required]
        public int Anio { get; set; }

        /// <summary>
        /// Indica si el Carro está disponible para alquiler.
        /// </summary>
        [Required]
        public bool Disponible { get; set; }

        /// <summary>
        /// Propiedad de navegación para la localidad del carro.
        /// </summary>
        [ForeignKey("Localidad")]
        public int LocalidadID { get; set; }
        public LocalidadModel Localidad { get; set; }
    }
}
