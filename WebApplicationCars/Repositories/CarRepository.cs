﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarRentalAPI.Models;
using Microsoft.EntityFrameworkCore; 
namespace CarRentalAPI.Repositories
{
    public class CarRepository : ICarRepository
    {
        private readonly CarRentalDbContext _context; 

        public CarRepository(CarRentalDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<List<CarModel>> GetAvailableCarsAsync(int pickupLocation, int returnLocation)
        {
            return await _context.Cars
                .Where(car => car.Disponible && car.LocalidadID == pickupLocation)
                .ToListAsync();
        }

        public async Task<CarModel> GetCarDetailsAsync(int id)
        {
            return await _context.Cars
                .FirstOrDefaultAsync(car => car.CarId == id);
        }

             
    }
}
