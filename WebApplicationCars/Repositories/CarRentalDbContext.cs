﻿using Microsoft.EntityFrameworkCore;
using CarRentalAPI.Models;

namespace CarRentalAPI.Repositories
{
    public class CarRentalDbContext : DbContext
    {
        public CarRentalDbContext(DbContextOptions<CarRentalDbContext> options) : base(options)
        {
        }

        public DbSet<LocalidadModel> Locations { get; set; }
        public DbSet<CarModel> Cars { get; set; }

        // Configuración de las entidades y relaciones 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CarModel>()
                .HasOne(c => c.Localidad)
                .WithMany(l => l.Cars)
                .HasForeignKey(c => c.LocalidadID);
        }
    }
}
