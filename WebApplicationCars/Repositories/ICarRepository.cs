﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarRentalAPI.Models;

namespace CarRentalAPI.Repositories
{
    /// <summary>
    /// Interfaz para el repositorio de Carros.
    /// </summary>
    public interface ICarRepository
    {
        /// <summary>
        /// Obtiene todos los Carros disponibles en una localidad de recogida y devolución específica.
        /// </summary>
        /// <param name="pickupLocation">ID de la localidad de recogida</param>
        /// <param name="returnLocation">ID de la localidad de devolución</param>
        /// <returns>Lista de vehículos disponibles</returns>
        Task<List<CarModel>> GetAvailableCarsAsync(int pickupLocation, int returnLocation);

        /// <summary>
        /// Obtiene los detalles de un Carro por su ID.
        /// </summary>
        /// <param name="id">ID del Carro</param>
        /// <returns>Detalles del Carro</returns>
        Task<CarModel> GetCarDetailsAsync(int id);


    }
}

